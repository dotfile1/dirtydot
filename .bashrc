#
# ~/.bashrc
#

[[ $- != *i* ]] && return

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Change the window title of X terminals
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
	fi

	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

unset use_color safe_term match_lhs sh

#Use vi keybind instead of emacs in the terminal
#set -o vi #canser atm
# Elinks aliases
	alias esearch='elinks duckduckgo.org'
	alias ewiki='elinks wiki.archlinux.org'

alias visudo='sudo EDITOR=vim visudo'	
alias dd='dd status=progress'					# progress
alias ll='ls -lAh'
alias cp="cp -i"  						# confirm before overwriting something
alias df='df -h'			                        # human-readable sizes
alias free='free -m'                      			# show sizes in MB
alias mountauresh='sudo mount -o uid=1000,gid=1000,dmask=022,fmask=133'
alias wiki='firefox /usr/share/doc/arch-wiki/html/en/' 
alias cdurxvt='urxvt -cd $PWD &'

#alias for my personal scripts
alias mountwindows='~/.scripts/MountWindows'
alias runaswine='~/.scripts/RunAsWine'
alias makepdf='~/.scripts/makepdf'

#ssh alias
alias sshphone='ssh -p 8022 u0_a67@192.168.1.22'
alias sshpi='ssh alarm@192.168.1.30'
alias sshlpi='ssh alarm@172.23.69.11'
alias ssherwan='ssh -p 22 erwan@195.154.118.79'
alias ssherwan2='ssh -p 1996 auresh@51.159.64.209'
#alias sshphone='ssh -p 8022 u0_a67@172.20.7.204'
complete -cf sudo

#Image rotation
#Rotate a picture 180°
alias rotate='convert -rotate "180"'

# Do not save a command twice
export HISTCONTROL=ignoredups

# Have less display colours
# from: https://wiki.archlinux.org/index.php/Color_output_in_console#man
export LESS=-R
export LESS_TERMCAP_mb=$'\e[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\e[1;33m'     # begin blink
export LESS_TERMCAP_so=$'\e[01;44;37m' # begin reverse video
export LESS_TERMCAP_us=$'\e[01;37m'    # begin underline
export LESS_TERMCAP_me=$'\e[0m'        # reset bold/blink
export LESS_TERMCAP_se=$'\e[0m'        # reset reverse video
export LESS_TERMCAP_ue=$'\e[0m'        # reset underline

# Enable history appending instead of overwriting.  #139609
#shopt -s histappend

#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#Image compression
#Recursively compress to a given percentage size a png or jpeg image
comppng()
{
	find . -name "*.png" | xargs pngquant --verbose --quality $1 --ext .png --force	
}

compjpeg()
{
	find . -name "*.jpg" | xargs jpegoptim --all-progressive --max=$1
}

# display a random wallpaper each time I start a terminal
#feh --bg-scale --randomize --no-fehbg /home/auresh/Desktop/wallpaper/*

#enable pass extention
export PASSWORD_STORE_ENABLE_EXTENSIONS="true"
#complete -W "code" pass
#complete -o filenames -F _pass pass #-W "code" pass 
