#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

#startx when tt1 logged
if [[ ! ${DISPLAY} && ${XDG_VTNR} == 1 ]]; then
	exec startx
fi

